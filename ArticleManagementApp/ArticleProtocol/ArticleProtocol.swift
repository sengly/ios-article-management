//
//  ArticleProtocol.swift
//  ArticleManagementApp
//
//  Created by Sengly Sun on 12/17/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
protocol  ArticleProtocol {
    func fetchAllArticle(completionHandler: @escaping (_ articleModel: [ArticleModel])->Void) 
}
