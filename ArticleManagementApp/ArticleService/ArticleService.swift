//
//  ArticleService.swift
//  ArticleManagementApp
//
//  Created by Sengly Sun on 12/17/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
class ArticleService: ArticleProtocol{
    func fetchAllArticle(completionHandler: @escaping ([ArticleModel]) -> Void) {
        var articleModel = [ArticleModel]()
        let url = URL(string: "http://110.74.194.124:15011/v1/api/articles")
        let request = URLRequest(url: url!)
        URLSession.shared.dataTask(with: request) { (data, respone, error) in
            if let error = error{
                print("Error: \(error.localizedDescription)")
            }
            guard let data = data else{
                print("NO DATA")
                return
            }
            
            let content = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            let res = content as! [String:Any]
            let articles = res["DATA"] as! NSArray
            for article in articles {
                let art = article as! [String: Any]
                let id = art["ID"] as! Int
                let title = art["TITLE"] as! String
                let description = art["DESCRIPTION"] as! String
                let createdDate = art["CREATED_DATE"] as! String
                let a = ArticleModel(id: id, title: title, description: description, createdDate: createdDate)
                articleModel.append(a)
            }
            completionHandler(articleModel)
            print("Article: \(articleModel)")
        }.resume()
    }
    
    
        
}
