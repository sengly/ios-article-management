//
//  ViewController.swift
//  ArticleManagementApp
//
//  Created by Sengly Sun on 12/17/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet var tableView: UITableView!
    var articleService: ArticleService!
    var articles = [ArticleModel]()

    //viewDidLoad : first function that run after view controller load
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableViewCell()
        getArticles()
        setUpGesture()
    }
    
    //function that return the number of row in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    //function that provide the cell based on number of row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        articleService = ArticleService()
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! ArticleTableViewCell
        cell.titleLabel.text = articles[indexPath.row].title
        cell.dateLabel.text = articles[indexPath.row].createdDate
        return cell
    }
    
    //function to register table view cell
    func setTableViewCell(){
        tableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "mycell")
    }
    
//    function to set up gesture
    func setUpGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tableView.addGestureRecognizer(tap)
        
    }

    @objc func tapGesture(_ tapGesture: UITapGestureRecognizer){
        print("Tapped")
        print(articles)
        let point = tapGesture.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: point){
            let storyboard = UIStoryboard(name: "ArticleDetailStoryboard", bundle: Bundle.main)
            let detailStoryboard = storyboard.instantiateViewController(identifier: "articleDetail") as! ArtilcleDetailViewController
            detailStoryboard.date = articles[indexPath.row].createdDate
            detailStoryboard.desc = articles[indexPath.row].description
            detailStoryboard.t = articles[indexPath.row].title
            navigationController?.pushViewController(detailStoryboard, animated: true)
        }
    }
//    function getting array of article
    func getArticles(){
        articleService = ArticleService()
        articleService.fetchAllArticle { (articles) in
            self.articles = articles
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    


}

