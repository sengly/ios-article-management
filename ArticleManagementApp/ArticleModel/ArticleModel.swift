//
//  ArticleModel.swift
//  ArticleManagementApp
//
//  Created by Sengly Sun on 12/17/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
class ArticleModel {
    
    var id: Int
    var title: String
    var description: String
    var createdDate: String
    
    init(id:Int,title:String,description:String,createdDate:String) {
        self.id = id
        self.title = title
        self.description = description
        self.createdDate = createdDate
    }
    
}
